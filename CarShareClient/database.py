#!/usr/bin/env python3
from modules import pg8000
import configparser
import bcrypt
from datetime import datetime, timedelta


# Define some useful variables
ERROR_CODE = 55929

#####################################################
##  Database Connect
#####################################################

def database_connect():
    # Read the config file
    config = configparser.ConfigParser()
    config.read('config.ini')

    # Create a connection to the database
    connection = None
    try:
        connection = pg8000.connect(database=config['DATABASE']['dbuser'],
            user=config['DATABASE']['user'],
            password=config['DATABASE']['password'],
            host=config['DATABASE']['host'])
    except pg8000.OperationalError as e:
        print("""Error, you haven't updated your config.ini or you have a bad
        connection, please try again. (Update your files first, then check
        internet connection)
        """)
        print(e)
    #return the connection to use
    return connection

#####################################################
##  Login
#####################################################

def check_login(email, password):
    # Dummy data
    # val = ['Shadow', 'Mr', 'Evan', 'Nave', '123 Fake Street, Fakesuburb', 'SIT', '01-05-2016', 'Premium', '1']
    # TODO
    # Check if the user details are correct!
    # Return the relevant information (watch the order!)
    conn = database_connect()
    if (conn is None):
        return ERROR_CODE
    cur = conn.cursor()
    try:
        cur.execute('''SELECT * FROM carsharing.fetch_password(%s);''', (email,))
        hashed_pass = cur.fetchone()
        # print(hashed_pass)
        '''
        Using the password entered by the user, it hashes the password using the original salt
        stored in the DB from there we can verify whether it matches the one in the DB record
        '''
        hashed_pass = bcrypt.hashpw(password.encode('utf-8'),hashed_pass[0].encode('utf-8')).decode('utf-8')
        cur.execute(''' SELECT * FROM carsharing.check_login(%s,%s);'''
                        , (email, hashed_pass))
        val = cur.fetchone()
        # print(val)
        cur.close()
        conn.close()
        return val
    except Exception as e:
        print(e)
    cur.close()
    conn.close()
    return None


#####################################################
##  Homebay
#####################################################
def update_homebay(email, bayname):
    # TODO
    # Update the user's homebay
    conn = database_connect()
    if (conn is None):
        return ERROR_CODE
    cur = conn.cursor()
    try:
        cur.execute( '''SELECT * FROM carsharing.update_my_homebay(%s,%s)''', (email, bayname))
        newBay = cur.fetchone()
        conn.commit()
        cur.close()
        conn.close()
        return [newBay, True]
    except Exception as e:
        print(e)
    cur.close()
    conn.close()
    return [-1, False]

#####################################################
##  Booking (make, get all, get details)
#####################################################

def make_booking(email, car_rego, date, hour, duration):
    # TODO
    # Insert a new booking
    # Make sure to check for:
    #       - If the member already has booked at that time
    #       - If there is another booking that overlaps
    #       - Etc.
    # return False if booking was unsuccessful :)
    # We want to make sure we check this thoroughly
    conn = database_connect()
    if conn is None:
        return ERROR_CODE
    cur = conn.cursor()
    memberid = None

    # starthour = hour + ":00:00"
    # endhour = str(int(hour) + int(duration)) + ":00:00"
    # print(endhour)
    startDateTime = datetime.strptime(date + hour, "%Y-%m-%d%H")
    duration = timedelta(hours = int(duration,10))
    endDateTime = startDateTime + duration
    startTime = startDateTime.strftime("%Y-%m-%d %H:00:00")
    endTime = endDateTime.strftime("%Y-%m-%d %H:00:00")

    querymemberid = ''' SELECT * FROM carsharing.fetch_memberid(%s);'''

    # querybooking = '''  INSERT INTO carsharing.Booking (car, madeBy, whenBooked, startTime, endTime)
    #                     VALUES (%s, %s, CURRENT_TIMESTAMP, %s::timestamp, %s::timestamp);'''
    # queryUpdateBookingCount = '''UPDATE carsharing.member SET stat_nrOfBookings = (SELECT stat_nrOfBookings FROM carsharing.member M WHERE UPPER(M.email) = UPPER(%s::varchar))::int + 1'''
    querybooking = ''' SELECT * FROM carsharing.make_booking(%s,%s,%s,%s,%s);'''
    try:
        # Get member ID before executing booking data insert query
        cur.execute(querymemberid, (email,))
        memberid = cur.fetchone()
        memberid = str(memberid[0])

        if (memberid == None):
            return False
        else:
            cur.execute(querybooking, (email, car_rego, memberid, startTime, endTime))
            # cur.execute(queryUpdateBookingCount, (email,))

    except Exception as e:
        cur.close()
        conn.close()
        print(e)
        return False
    conn.commit()
    cur.close()
    conn.close()
    return True


def get_all_bookings(email):
    # val = [['66XY99', 'Ice the Cube', '01-05-2016', '10', '4', '29-04-2016'],['66XY99', 'Ice the Cube', '27-04-2016', '16'], ['WR3KD', 'Bob the SmartCar', '01-04-2016', '6']]
    # TODO
    # Get all the bookings made by this member's email
    # cast(b.whenBooked as time)
    conn = database_connect()
    if conn is None:
        return ERROR_CODE
    cur = conn.cursor()
    query = '''SELECT * FROM carsharing.get_all_car_bookings(%s)'''
    try:
        cur.execute(query, (email,))
        val = cur.fetchall()
        cur.close()
        conn.close()
        return val
    except Exception as e:
        print(e)
    cur.close()
    conn.close()
    return None


def get_booking(b_date, b_hour, car):
    conn = database_connect()
    if conn is None:
        return ERROR_CODE
    cur = conn.cursor()

    query = '''SELECT * FROM carsharing.get_the_booking_details(%s, %s, %s);'''
    try:
        cur.execute(query, (b_date, b_hour, car))
        val = cur.fetchone()
        cur.close()
        conn.close()
        return val

    except Exception as e:
        print(e)
    cur.close()
    conn.close()
    return None


#####################################################
##  Car (Details and List)
#####################################################

def get_car_details(regno):
    # val = ['66XY99', 'Ice the Cube','Nissan', 'Cube', '2007', 'auto', 'Luxury', '5', 'SIT', '8', 'http://example.com']
    # TODO
    # Get details of the car with this registration number
    # Return the data (NOTE: look at the information, requires more than a simple select. NOTE ALSO: ordering of columns)
    conn = database_connect()
    if conn is None:
        return ERROR_CODE
    cur = conn.cursor()
    query = ''' SELECT * FROM carsharing.get_car_details(%s);'''
    try:
        cur.execute(query, (regno,))
        val = cur.fetchone()
        cur.close()
        conn.close()
        return val
    except Exception as e:
        print(e)
    cur.close()
    conn.close()
    return None

def get_all_cars():
    # val = [ ['66XY99', 'Ice the Cube', 'Nissan', 'Cube', '2007', 'auto'], ['WR3KD', 'Bob the SmartCar', 'Smart', 'Fortwo', '2015', 'auto']]
    # TODO
    # Get all cars that PeerCar has
    # Return the results
    conn = database_connect()
    if conn is None:
        return ERROR_CODE
    cur = conn.cursor()
    query = ''' SELECT * FROM carsharing.get_all_cars();'''
    try:
        cur.execute(query)
        val = cur.fetchall()
        cur.close()
        conn.close()
        return val
    except Exception as e:
        print(e)
    cur.close()
    conn.close()
    return None

def get_available_times(car):
    conn = database_connect()
    if conn is None:
        return ERROR_CODE
    cur = conn.cursor()
    available = [True] * 24;
    query = ''' SELECT * FROM carsharing.get_availability_day(%s::varchar);'''
    try:
        cur.execute(query, (car,))
        val = cur.fetchall()
        pointer = 0
        for each in val:
          duration = (val[pointer][1]-val[pointer][0]).total_seconds()/3600
          duration = int(duration)
          for count in range(val[pointer][1].hour,min(range(val[pointer][1].hour + duration, 25))):
            available[count] = False
          pointer+=1
        print(available)
        cur.close()
        conn.close()
        return available
    except Exception as e:
        print(e)
    cur.close()
    conn.close()
    return None

#####################################################
##  Bay (detail, list, finding cars inside bay)
#####################################################

def get_prev_level(location):
    conn = database_connect()

    if conn is None:
        return ERROR_CODE
    cur = conn.cursor()
    query = ''' SELECT * FROM carsharing.get_previous_level(%s)
                ;''' # still working on this
    try:
        cur.execute(query, (location,))
        val = cur.fetchall()
        cur.close()
        conn.close()
        return val
    except Exception as e:
        print(e)
    cur.close()
    conn.close()
    return None

def get_level(level):
    conn = database_connect()

    if conn is None:
        return ERROR_CODE
    cur = conn.cursor()
    query = ''' SELECT * FROM carsharing.get_next_level(%s);'''
    try:
        cur.execute(query, (level,))
        val = cur.fetchall()
        cur.close()
        conn.close()
        return val
    except Exception as e:
        print(e)
    cur.close()
    conn.close()
    return None

def get_all_bays(location):
    # val = [['SIT', '123 Some Street, Boulevard', '2'], ['some_bay', '1 Somewhere Road, Right here', '1']]
    # TODO
    # Get all the bays that PeerCar has :)
    # And the number of bays
    # Return the results
    conn = database_connect()
    if conn is None:
        return ERROR_CODE
    cur = conn.cursor()

    query = ''' SELECT * FROM carsharing.get_all_bays(%s)'''
    try:
        # cur.execute(getid, (location,))
        # locationid = cur.fetchone()
        # print(locationid)
        cur.execute(query, (location,))
        val = cur.fetchall()
        cur.close()
        conn.close()
        return val
    except Exception as e:
        print("SQL error!")
        print(e)
    cur.close()
    conn.close()
    return None

def get_bay(name):
    # val = ['SIT', 'Home to many (happy?) people.', '123 Some Street, Boulevard', '-33.887946', '151.192958']
    conn = database_connect()
    if (conn is None):
        return ERROR_CODE
    cur = conn.cursor()
    query = None
    if (name.isdigit()):
        query = ''' SELECT * FROM carsharing.get_bay_with_id(%s);'''

    else:
        query = ''' SELECT * FROM carsharing.get_bay_with_name(%s);'''

    try:
        cur.execute(query, (name,))
        val = cur.fetchone() #As the name is not necessarily unique, in case of conflict, we will fetch the one with the highest walkscore
        cur.close()
        conn.close()
        return val;
    except Exception as e:
        print(e)
    cur.close()
    conn.close()
    #TEST

    return val

def search_bays(search_term):
    # val = [['SIT', '123 Some Street, Boulevard', '-33.887946', '151.192958']]
    # TODO
    # What if the search term is latitude and longitude?
    # Could be improved with partial string matching (EG missing letters, ignoring spaces in input or weird characters)
    # rank by similarity to search ter. This could be done by fetching all the results, processing them in python, then returning the best ones.
    # not sure if we could do it more efficiently?

    # Select the bays that match (or are similar) to the search term
    # You may like this
    conn = database_connect()
    regex_gps = '''^([\-\+]?)([0123456789]+)([.])([0123456789]+)( *, )( *)([\-\+]?)([0123456789]+)([.])([0123456789]+)'''

    if (conn is None):
        return ERROR_CODE
    cur = conn.cursor()
    gps_pos = search_term + '%'
    search_term = '%' + search_term + '%'

    query = ''' SELECT * FROM carsharing.search_bays(%s,%s);'''

    try:
        cur.execute(query, (search_term, gps_pos))
        val = cur.fetchall()
        cur.close()
        conn.close()
        return val;
    except Exception as e:
        print(e)


    cur.close()
    conn.close()
    return val

def get_cars_in_bay(bay_name):
    # val = [ ['66XY99', 'Ice the Cube'], ['WR3KD', 'Bob the SmartCar']]
    # TODO
    # Get the cars inside the bay with the bay name
    # Cars who have this bay as their bay :)
    # Return simple details (only regno and name)
    conn = database_connect()

    if (conn is None):
        return ERROR_CODE

    cur = conn.cursor()
    # print(bay_name)
    if (bay_name.isdigit()):
        query = ''' SELECT * FROM carsharing.get_cars_in_bay_id(%s)'''
    else:
        query = ''' SELECT * FROM carsharing.get_cars_in_bay_name(%s)'''
    try:
        cur.execute(query, (bay_name,))
        val = cur.fetchall() #
        conn.commit()
        cur.close()
        conn.close()
        return val;
    except Exception as e:
        print(e)

    cur.close()
    conn.close()
    return None
