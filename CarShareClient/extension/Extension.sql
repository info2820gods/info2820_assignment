--creates table existing_bookings using the booking table. No need for any INSERT query, data is auto populated
--CREATE MATERIALIZED VIEW carsharing.existing_bookings
CREATE TABLE carsharing.existing_bookings
  AS (SELECT car, starttime, endtime FROM carsharing.booking b);

create index on carsharing.existing_bookings(starttime);
create index on carsharing.existing_bookings(endtime);
create index on carsharing.existing_bookings(car);



--Avoids Duplicate Booking
CREATE FUNCTION carsharing.Abort_Booking() RETURNS trigger AS $$
BEGIN

IF new.car IN
    (SELECT car FROM carsharing.existing_bookings
    WHERE (starttime >= new.starttime) AND (starttime <= new.endTime))
  THEN
  RAISE EXCEPTION 'The car is not available for booking for the specified period. Please choose another car or different booking times.';
  END IF;
  RETURN NEW;
END
$$ LANGUAGE plpgsql;
CREATE TRIGGER Avoid_Double_Booking
BEFORE INSERT ON carsharing.booking
FOR EACH ROW
EXECUTE PROCEDURE Abort_Booking();



--Inserts a row in existing_bookings whenever a new row in booking is inserted
CREATE FUNCTION carsharing.booking_insert() RETURNS TRIGGER AS $$

BEGIN
		INSERT INTO carsharing.existing_bookings
		values (new.car,new.startTime, new.endtime);
	RETURN NEW;
END
$$ LANGUAGE plpgsql;
CREATE TRIGGER Populate_Existing_Bookings
AFTER INSERT ON carsharing.booking
FOR EACH ROW
EXECUTE PROCEDURE booking_insert();
