'''
Run this once only
'''
import pg8000
import bcrypt
import configparser

def database_connect():
    # Read the config file
    config = configparser.ConfigParser()
    config.read('config.ini')

    # Create a connection to the database
    connection = None
    try:
        connection = pg8000.connect(database=config['DATABASE']['user'],
            user=config['DATABASE']['user'],
            password=config['DATABASE']['password'],
            host=config['DATABASE']['host'])
    except pg8000.OperationalError as e:
        print("""Error, you haven't updated your config.ini or you have a bad
        connection, please try again. (Update your files first, then check
        internet connection)
        """)
        print(e)
    #return the connection to use
    return connection

# Formats the table to accomodate for the longer hash string and removes redundant column
def formatTable():
    conn = database_connect()
    if conn is None:
        print("No connection")
        return
    cur = conn.cursor()
    try:
        cur.execute('''ALTER TABLE carsharing.member ALTER COLUMN password TYPE VARCHAR(60);''')
        cur.execute('''ALTER TABLE carsharing.member DROP COLUMN pw_salt;''')
        conn.commit()
    except Exception as e:
        print(e)
    cur.close()
    conn.close()

# Mainly used for debugging purposes so that the users can be tested
def keepUnhashPass():
    conn = database_connect()
    if conn is None:
        print("No connection")
        return
    cur = conn.cursor()
    try:
        cur.execute('''ALTER TABLE carsharing.member ADD COLUMN unhashed_pw VARCHAR(60);''')
        cur.execute('''UPDATE carsharing.member SET unhashed_pw = password;''')
        conn.commit()
    except Exception as e:
        print(e)
    cur.close()
    conn.close()

def main():
    formatTable() # formats table so hashed password can fit

    decision = input("Do you want to keep unhashed passwords?(y/n) ")
    if decision == 'y':
        print("Unhashed passwords will be copied to unhashed_pw column")
        keepUnhashPass()

    conn = database_connect()
    if conn is None:
        print("No connection")
        return
    cur = conn.cursor()
    # Grab all passwords from the DB
    cur.execute('''SELECT email,password FROM carsharing.member order by memberno;''')
    userLogin = cur.fetchall()

    # Hashing and salting each password for each user
    for user in userLogin:
        user[1] = bcrypt.hashpw(user[1].encode('utf-8'), bcrypt.gensalt()).decode('utf-8')
        print(user[1])
        cur.execute('''UPDATE carsharing.member SET password = %s WHERE email = %s''', (user[1],user[0]))

    print("All passwords have been hashed")
    conn.commit()
    cur.close()
    conn.close()

if __name__ == "__main__":
    main()
