--get_all_car_bookings SP with call
--get_the_booking_detail SP with call
--updat_my_homebay SP with call

--get all bookings for a member

CREATE OR REPLACE FUNCTION carsharing.get_all_car_bookings(email varchar)
RETURNS TABLE(registration regotype, name varchar, startdate date, duration int) AS $BODY$
BEGIN
  RETURN QUERY  SELECT c.regno, c.name, date(b.startTime) as startdate, ((EXTRACT(epoch FROM (SELECT (b.endTime - b.startTime)))::int)/3600)::int as duration
                FROM booking b INNER JOIN car c ON (car=regno) INNER JOIN MEMBER M ON (madeby=memberno)
                WHERE UPPER(M.email)=UPPER($1)
                ORDER BY startdate DESC;
END
$BODY$
LANGUAGE plpgsql;

--get booking details for a single booking for a memeber

CREATE OR REPLACE FUNCTION carsharing.get_the_booking_details(b_date date, b_hour int, car varchar)
RETURNS TABLE(nickname varchar, reg regotype, name varchar, startTime date, bhour int, duration int, whenBooked date, bayname varchar) AS $BODY$
BEGIN
  RETURN QUERY SELECT m.nickname, b.car, c.name, date(b.startTime), EXTRACT(hour from b.whenBooked)::int, (EXTRACT(epoch FROM (SELECT (b.endTime - b.startTime)))/3600)::int, date(b.whenBooked), cb.name
               FROM booking b join member m on (madeby=memberno) join  car c on (b.car=regno) join carbay cb on (parkedAt=bayID)
               WHERE date(b.startTime) = $1 AND (EXTRACT(epoch FROM (SELECT (b.endTime - b.startTime)))/3600) = $2 AND b.car=$3;
END
$BODY$
LANGUAGE plpgsql;

--update the homebay

CREATE OR REPLACE FUNCTION carsharing.update_my_homebay(p_email varchar, bayname varchar) RETURNS int AS
$BODY$
DECLARE
id int;
BEGIN
  SELECT C1.bayID INTO id FROM carsharing.carBay C1
                 WHERE C1.name = bayname
                 ORDER BY walkscore;
    UPDATE carsharing.member
    SET homeBay = id
    WHERE UPPER(email) = UPPER($1);
return id;
 END
$BODY$
LANGUAGE plpgsql;

-- CHECK LOGIN
CREATE OR REPLACE FUNCTION carsharing.check_login(p_email varchar, p_password varchar)
RETURNS TABLE(nickname varchar,nametitle varchar,namegiven varchar,namefamily varchar,address varchar,
            homeBay int,birthdate date,subscribed varchar, stat_nrofbookings int) AS $$
BEGIN
    RETURN QUERY  SELECT M.nickname, M.nametitle, M.namegiven, M.namefamily, M.address, M.homeBay,
                        M.birthdate, M.subscribed, M.stat_nrofbookings
                  FROM carsharing.member M
                  WHERE UPPER(M.email) = UPPER($1) AND M.password = $2;
END;
$$ LANGUAGE plpgsql;

-- GET CAR DETAILS
CREATE OR REPLACE FUNCTION carsharing.get_car_details(p_regno regotype)
RETURNS TABLE(regno regotype,name varchar,make varchar,model varchar, year int, transmission varchar,
    category varchar,capacity int,bayname varchar,walkscore int,mapurl varchar) AS $$
BEGIN
    RETURN QUERY  SELECT C.regno,C.name,C.make,C.model,C.year,C.transmission,
                          M.category,M.capacity,B.name,B.walkscore,B.mapurl
                  FROM carsharing.car C NATURAL JOIN carsharing.carmodel M JOIN carbay B on(parkedat = bayid)
                  WHERE C.regno = $1;
END;
$$ LANGUAGE plpgsql;

-- GET ALL CARS
CREATE OR REPLACE FUNCTION carsharing.get_all_cars()
RETURNS TABLE(regno regotype,name varchar, make varchar,model varchar, year int, transmission varchar) AS $$
BEGIN
    RETURN QUERY  SELECT C.regno,C.name,C.make,C.model,C.year,C.transmission
                  FROM carsharing.car C;
END;
$$ LANGUAGE plpgsql;

-- GET CARS IN BAY WITH NAME
CREATE OR REPLACE FUNCTION carsharing.get_cars_in_bay_name(p_bayName varchar)
RETURNS TABLE(regno regotype,name varchar) AS $$
BEGIN
    RETURN QUERY  SELECT C1.regno, C1.name
                  FROM Car C1
                    INNER JOIN CarBay C2 ON (parkedAt = bayID)
                  WHERE C2.name = $1
                  ORDER BY C1.name, C1.regno;
END;
$$ LANGUAGE plpgsql;

-- GET CARS IN BAY WITH ID
CREATE OR REPLACE FUNCTION carsharing.get_cars_in_bay_id(p_bayid int)
RETURNS TABLE(regno regotype,name varchar) AS $$
BEGIN
    RETURN QUERY  SELECT C1.regno, C1.name
                  FROM Car C1
                    INNER JOIN CarBay C2 ON (parkedAt = bayID)
                  WHERE C2.bayID = $1
                  ORDER BY C1.name, C1.regno;
END;
$$ LANGUAGE plpgsql;

-- GET PREVIOUS LEVEL
CREATE OR REPLACE FUNCTION carsharing.get_previous_level(locationname varchar)
RETURNS TABLE(name varchar) AS $$
BEGIN
  RETURN QUERY SELECT L2.name FROM carsharing.location L1
                INNER JOIN carsharing.location L2 ON(L2.locID = L1.is_at)
                WHERE L1.name = locationname;

END;
$$ LANGUAGE plpgsql;

-- GET NEXT LEVEL
CREATE OR REPLACE FUNCTION carsharing.get_next_level(locationname varchar)
RETURNS TABLE(name varchar) AS $$
BEGIN
  RETURN QUERY SELECT L2.name FROM carsharing.location L1
                INNER JOIN carsharing.location L2 ON(L2.is_at = L1.locID)
                WHERE L1.name = locationname;

END;
$$ LANGUAGE plpgsql;

-- GET ALL BAYS
CREATE OR REPLACE FUNCTION carsharing.get_all_bays(locationname varchar)
RETURNS TABLE(name varchar, address varchar, carcount bigint, map varchar) AS $$
DECLARE
locationid int;
BEGIN
  SELECT L1.locID INTO locationid
  FROM carsharing.location L1
    WHERE L1.name LIKE (locationname)::varchar;

  RETURN QUERY SELECT C1.name, C1.address, count(C2.regno), C1.mapurl
                FROM carsharing.carbay C1
                LEFT OUTER JOIN carsharing.Car C2 ON (C2.parkedAt = C1.bayID)
                LEFT OUTER JOIN carsharing.location L1 ON (L1.locID = C1.located_At)
                WHERE L1.locID IN (
                    WITH RECURSIVE location_hierarchy AS (
                        SELECT locid, ARRAY[]::integer[] AS parent_location
                        FROM carsharing.location WHERE is_at IS NULL

                        UNION ALL

                        SELECT carsharing.location.locid, location_hierarchy.parent_location || carsharing.location.is_at
                        FROM carsharing.location, location_hierarchy
                        WHERE carsharing.location.is_at = location_hierarchy.locid
                    ) SELECT locid FROM location_hierarchy WHERE locationid = ANY(location_hierarchy.parent_location)
                ) OR L1.locID = locationid

                GROUP BY C1.name, C1.address, C1.mapurl
                ORDER BY C1.name
                ;

END;
$$ LANGUAGE plpgsql;

-- GET BAY WITH ID
CREATE OR REPLACE FUNCTION carsharing.get_bay_with_id(bayidentifier int)
RETURNS TABLE(name varchar, description text, address varchar, gps_lat FLOAT, gps_long FLOAT) AS $$
BEGIN
  RETURN QUERY SELECT C1.name, C1.description, C1.address, C1.gps_lat, C1.gps_long FROM carsharing.carbay C1
                 WHERE bayID = bayidentifier
                 ORDER BY walkscore;

END;
$$ LANGUAGE plpgsql;

-- GET BAY WITH NAME
CREATE OR REPLACE FUNCTION carsharing.get_bay_with_name(bayname varchar)
RETURNS TABLE(name varchar, description text, address varchar, gps_lat FLOAT, gps_long FLOAT) AS $$
BEGIN
  RETURN QUERY SELECT C1.name, C1.description, C1.address, C1.gps_lat, C1.gps_long FROM carsharing.carBay C1
                 WHERE C1.name = bayname
                 ORDER BY walkscore;

END;
$$ LANGUAGE plpgsql;

-- SEARCH BAYS
CREATE OR REPLACE FUNCTION carsharing.search_bays(search_term varchar, gps_pos varchar)
RETURNS TABLE(name varchar, address varchar, cars_count bigint, gps_lat FLOAT, gps_long FLOAT) AS $$
BEGIN
  RETURN QUERY SELECT C1.name, C1.address, COUNT(C2.regno), C1.mapurl FROM carsharing.CarBay C1
                INNER JOIN carsharing.Car C2 ON (C2.parkedAt = C1.bayID)
                WHERE UPPER(C1.name) LIKE UPPER(search_term)
                OR UPPER(C1.description) LIKE UPPER(search_term)
                OR UPPER(C1.address) LIKE UPPER(search_term)
                OR (C1.gps_lat || ', ' || C1.gps_long) LIKE gps_pos
                GROUP BY C1.name, C1.address, C1.mapurl, C1.walkscore
                ORDER BY C1.walkscore
                LIMIT 10;

END;
$$ LANGUAGE plpgsql;

-- FETCH PASSWORD OF MEMBER
CREATE OR REPLACE FUNCTION carsharing.fetch_password(p_email varchar)
RETURNS TABLE(password varchar) AS $$
BEGIN
  RETURN QUERY SELECT M.password FROM carsharing.member M WHERE UPPER(M.email) = UPPER($1);
END;
$$ LANGUAGE plpgsql;

-- FETCH CAR AVAILABILITY FOR DAY
CREATE OR REPLACE FUNCTION carsharing.get_availability_day(car varchar)
RETURNS TABLE(starthour TIMESTAMP, endhour TIMESTAMP) AS $$
BEGIN
  RETURN QUERY SELECT E.starttime, E.endtime FROM carsharing.existing_bookings E WHERE UPPER(E.car) = UPPER($1) AND CURRENT_DATE >= DATE(E.starttime) AND CURRENT_DATE <= DATE(E.endtime);
END;
$$ LANGUAGE plpgsql;

-- FETCH ID OF MEMBER
CREATE OR REPLACE FUNCTION carsharing.fetch_memberid(p_email varchar)
RETURNS TABLE(memberNo int) AS $$
BEGIN
  RETURN QUERY SELECT M.memberNo FROM carsharing.Member M WHERE UPPER(M.email) = UPPER($1);
END;
$$ LANGUAGE plpgsql;

-- MAKE BOOKING
CREATE OR REPLACE FUNCTION carsharing.make_booking(p_email varchar,car_rego regotype, memberid int, startTime timestamp, endTime timestamp)
RETURNS boolean AS $$
BEGIN
  INSERT INTO carsharing.Booking (car, madeBy, whenBooked, startTime, endTime)
                      VALUES ($2, $3, CURRENT_TIMESTAMP, $4::timestamp, $5::timestamp);
  UPDATE carsharing.member SET stat_nrOfBookings = (SELECT stat_nrOfBookings FROM carsharing.member M WHERE UPPER(M.email) = UPPER($1::varchar))::int + 1;
  RETURN TRUE;
END;
$$ LANGUAGE plpgsql;
--select check_login('DrdrfosterFoster@gmail.com','$2b$12$ybP3T5cTrLhcWwhZ5zBfN.dRk62rbUWDUdj22TyoPCG.rYgt7zoDC');
--select get_car_details('564AEC');
--select get_all_cars();
--select get_cars_in_bay('Erskineville - Erskineville Road');

-- QUERY MEMBER ID
